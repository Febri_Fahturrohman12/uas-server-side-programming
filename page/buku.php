<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: ../index.php");
}

include_once "../api/routes.php";
include '../api/koneksi.php';
include "header.php";
?>
<div class="col-lg-2 col-md-2" style="padding-left: 0px;background-color: #313644;height: 100%;float: left;">
    <?php include "sidebar.php"; ?>
</div>
<div class="col-lg-10 col-md-10" style="background-color: #f3f4f3;height: 100%;float: right;">
    <div class="row">
        <div class="col-lg-12 col-md-12"
            style="background-color: #fff;height: 75px;vertical-align: middle;font-size: 19px;font-weight: 600;color: #727272;">
            <p style="vertical-align: middle;padding-top:25px;">Data Buku</p>
        </div>
        <div class="col-lg-12 col-md-12 mt-4">
            <div class="text-right mt-0 mb-0">
                <button data-toggle="modal" data-target="#modaltambah"  class="btn btn-primary btn-sm mb-3">Tambah Data Buku</button>
            </div>
            <!--Modal untuk tambah data buku-->
            <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Buku</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="../api/buku.php?proses=tambah&id=" method="post">
                                <div class="formgroup">
                                    <label>Kode Buku</label>
                                    <input type="text" name="kode" class="form-control" placeholder="Silahkan Masukan Kode Buku">
                                </div>
                                <div class="formgroup">
                                    <label>Judul Buku</label>
                                    <input type="text" name="judul" class="form-control" placeholder="Silahkan Masukan Judul Buku">
                                </div>
                                <div class="formgroup">
                                    <label>Penulis</label>
                                    <input type="text" name="penulis" class="form-control" placeholder="Silahkan Masukan Nama Penulis Buku">
                                </div>
                                <div class="formgroup">
                                    <label>Tahun Terbit</label>
                                    <input type="year" name="tahun_penerbit" class="form-control" placeholder="Silahkan Masukan Tahun Terbit">
                                </div>
                                <div class="formgroup">
                                    <label>Penerbit</label>
                                    <input type="text" name="penerbit" class="form-control" placeholder="Silahkan Masukan Penerbit">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12" style="padding:15px;background: #fff;">
                <table class="table table-striped" id="example" style="width:100%">
                    <!--menggunakan datatable client side-->
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Buku</th>
                            <th>Judul Buku</th>
                            <th>Penulis</th>
                            <th>Tahun Terbit</th>
                            <th>Penerbit</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=1;
                            $sql = "SELECT id as idbuku, kode, judul, penulis, penerbit, tahun_penerbit FROM buku";
                            $data = $conn->query($sql);
                            foreach($data as $hasil){ ?>
                        <tr>
                            <td><?=$no++; ?></td>
                            <td><?=$hasil['kode'];?></td>
                            <td><?=$hasil['judul'];?></td>
                            <td><?=$hasil['penulis'];?></td>
                            <td><?=$hasil['tahun_penerbit'];?></td>
                            <td><?=$hasil['penerbit'];?></td>
                            <td>
                                <button data-toggle="modal" data-target="#modaledit<?=$hasil['idbuku']; ?>"
                                    class="btn btn-warning" style="margin-top:0px;">Edit</button>
                                    <div class="modal fade" id="modaledit<?=$hasil['idbuku']; ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../api/buku.php?proses=edit&id=<?=$hasil['idbuku'];?>"
                                                        method="post">
                                                        <div class="formgroup">
                                                            <label>Kode Buku</label>
                                                            <input type="text" name="kode"
                                                                value="<?=$hasil['kode'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan Kode Buku">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Judul Buku</label>
                                                            <input type="text" name="judul"
                                                                value="<?=$hasil['judul'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan Nama Buku">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Penulis Buku</label>
                                                            <input type="text" name="penulis"
                                                                value="<?=$hasil['penulis'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan Penulis Buku">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Tahun Terbit</label>
                                                            <input type="text" name="tahun_penerbit" class="form-control"
                                                                value="<?=$hasil['tahun_penerbit'];?>"
                                                                placeholder="Silahkan Masukan Tahun Terbit">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Penerbit</label>
                                                            <input type="text" name="penerbit" class="form-control"
                                                                value="<?=$hasil['penerbit'];?>"
                                                                placeholder="Silahkan Masukan Penerbit">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="../api/buku.php?proses=hapus&id=<?=$hasil['idbuku'];?>" class="btn btn-danger" style="margin-top:0px;">Hapus</button>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
</div>