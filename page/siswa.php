<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: ../index.php");
}

include_once "../api/routes.php";
include '../api/koneksi.php';
include "header.php";
?>
<div class="col-lg-2 col-md-2" style="padding-left: 0px;background-color: #313644;height: 100%;float: left;">
    <?php include "sidebar.php"; ?>
</div>
<div class="col-lg-10 col-md-10" style="background-color: #f3f4f3;height: 100%;float: right;">
    <div class="row">
        <div class="col-lg-12 col-md-12" style="background-color: #fff;height: 75px;vertical-align: middle;font-size: 19px;font-weight: 600;color: #727272;">
            <p style="vertical-align: middle;padding-top:25px;">Data Siswa</p>
        </div>
        <div class="col-lg-12 col-md-12 mt-4">
            <div class="text-right mt-0 mb-0">
                <button data-toggle="modal" data-target="#modaltambah"  class="btn btn-primary btn-sm mb-3 align-right">Tambah Data Siswa</button>
            </div>
            <!--Modal untuk tambah data siswa-->
            <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Siswa</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="../api/siswa.php?proses=tambah&id=" method="post">
                            <div class="formgroup">
                                <label>NIS</label>
                                <input type="text" name="nis" class="form-control" placeholder="Silahkan Masukan NIS">
                            </div>
                            <div class="formgroup">
                                <label>Nama Siswa</label>
                                <input type="text" name="nama" class="form-control" placeholder="Silahkan Masukan Nama Siswa">
                            </div>
                            <div class="formgroup">
                                <label>Kelas</label>
                                <input type="text" name="kelas" class="form-control" placeholder="Silahkan Masukan Kelas Siswa">
                            </div>
                            <div class="formgroup">
                                <label>Gender</label>
                                <select name="jk" class="form-control">
                                    <option value="Laki">Laki Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12" style="padding:15px;background: #fff;">
                <table class="table table-striped" id="example" style="width:100%">
                    <!--menggunakan datatable client side-->
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIS</th>
                            <th>Nama Siswa</th>
                            <th>Kelas</th>
                            <th>Gender</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=1;
                            $sql = "SELECT id as idsiswa, nis, nama, jk, kelas FROM siswa";
                            $data = $conn->query($sql);
                            foreach($data as $hasil){ ?>
                        <tr>
                            <td><?=$no++; ?></td>
                            <td><?=$hasil['nis'];?></td>
                            <td><?=$hasil['nama'];?></td>
                            <td><?=$hasil['kelas'];?></td>
                            <td><?=$hasil['jk'];?></td>
                            <td>
                                <button data-toggle="modal" data-target="#modaledit<?=$hasil['idsiswa']; ?>"
                                    class="btn btn-warning" style="margin-top:0px;">Edit</button>
                                    <div class="modal fade" id="modaledit<?=$hasil['idsiswa']; ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../api/siswa.php?proses=edit&id=<?=$hasil['idsiswa'];?>"
                                                        method="post">
                                                        <div class="formgroup">
                                                            <label>NIS</label>
                                                            <input type="text" name="nis"
                                                                value="<?=$hasil['nis'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan NIS">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Nama Siswa</label>
                                                            <input type="text" name="nama"
                                                                value="<?=$hasil['nama'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan Nama Siswa">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Kelas</label>
                                                            <input type="text" name="kelas"
                                                                value="<?=$hasil['kelas'];?>" class="form-control"
                                                                placeholder="Silahkan Masukan Kelas Siswa">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Gender</label>
                                                            <select name="jk" class="form-control">
                                                                <option <?=$hasil['jk'] == 'Laki' ? 'selected':''; ?> value="Laki">Laki Laki</option>
                                                                <option <?=$hasil['jk'] == 'Perempuan' ? 'selected':''; ?> value="Perempuan">Perempuan</option>
                                                            </select>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="../api/siswa.php?proses=hapus&id=<?=$hasil['idsiswa'];?>" class="btn btn-danger" style="margin-top:0px;">Hapus</button>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
</div>