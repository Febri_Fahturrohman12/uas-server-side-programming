<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: ../index.php");
}


include_once "../api/routes.php";
include '../api/koneksi.php';
include "header.php";
?>
<div class="col-lg-2 col-md-2" style="padding-left: 0px;background-color: #313644;height: 100%;float: left;">
    <?php include "sidebar.php"; ?>
</div>
<div class="col-lg-10 col-md-10" style="background-color: #f3f4f3;height: 100%;float: right;">
    <div class="row">
        <div class="col-lg-12 col-md-12" style="background-color: #fff;height: 75px;vertical-align: middle;font-size: 19px;font-weight: 600;color: #727272;">
            <p style="vertical-align: middle;padding-top:25px;">Data Peminjaman</p>
        </div>
        <div class="col-lg-12 col-md-12 mt-4">
            <div class="text-right mt-0 mb-0">
                <button data-toggle="modal" data-target="#modaltambah"  class="btn btn-primary btn-sm mb-3">Tambah Data Peminjaman</button>
            </div>
            <!--Modal untuk tambah data buku-->
            <div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">  
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Peminjaman</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="../api/peminjaman.php?proses=tambah&id=" method="post">
                                <div class="formgroup">
                                    <label>Pilih Buku</label>
                                    <select class="form-control select2bs4" name="id_buku" style="width:100%;">
                                        <?php
                                        $sql = "SELECT id as id_buku, kode, judul, penulis, penerbit, tahun_penerbit FROM buku";
                                        $data = $conn->query($sql);
                                        foreach($data as $hasil){ ?>
                                            <option value="<?=$hasil['id_buku'];?>"><?php echo $hasil['kode']." - ".$hasil['judul'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="formgroup">
                                    <label>Pilih Siswa</label>
                                    <select class="form-control select2bs4" name="id_siswa" style="width:100%;">
                                        <?php
                                        $sql = "SELECT id as id_siswa, nis, nama, jk, kelas FROM siswa";
                                        $data = $conn->query($sql);
                                        foreach($data as $hasil){ ?>
                                            <option value="<?=$hasil['id_siswa'];?>"><?php echo $hasil['nis']." - ".$hasil['nama'];?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="formgroup">
                                    <label>Tanggal Pinjam</label>
                                    <input type="date" id="tanggal_pinjam" class="form-control" name="tanggal_pinjam">
                                </div>
                                <div class="formgroup">
                                    <label>Tanggal Kembali</label>
                                    <input type="date" id="tanggal_kembali" class="form-control" name="tanggal_kembali">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12" style="padding:15px;background: #fff;">
                <table class="table table-striped" id="example" style="width:100%">
                    <!--menggunakan datatable client side-->
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Buku</th>
                            <th>Judul Buku</th>
                            <th>Peminjam</th>
                            <th>Tanggal Pinjam</th>
                            <th>Tanggal Kembali</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=1;
                            $sql = "SELECT peminjaman.id as idpeminjaman, peminjaman.tanggal_pinjam,peminjaman.tanggal_kembali,peminjaman.id_buku,peminjaman.id_siswa,buku.kode,buku.judul,siswa.nis,siswa.nama FROM peminjaman
                            INNER JOIN buku ON buku.id = peminjaman.id_buku INNER JOIN siswa ON siswa.id = peminjaman.id_siswa";
                            $data = $conn->query($sql);
                            foreach($data as $hasil){ ?>
                        <tr>
                            <td><?=$no++; ?></td>
                            <td><?=$hasil['kode'];?></td>
                            <td><?=$hasil['judul'];?></td>
                            <td><?=$hasil['nama'];?></td>
                            <td><?=$hasil['tanggal_pinjam'];?></td>
                            <td><?=$hasil['tanggal_kembali'];?></td>
                            <td>
                                <button data-toggle="modal" data-target="#modaledit<?=$hasil['idpeminjaman']; ?>"
                                    class="btn btn-warning" style="margin-top:0px;">Edit</button>
                                    <div class="modal fade" id="modaledit<?=$hasil['idpeminjaman']; ?>">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="../api/peminjaman.php?proses=edit&id=<?=$hasil['idpeminjaman'];?>"
                                                        method="post">
                                                        <div class="formgroup">
                                                            <label>Pilih Buku</label>
                                                            <select class="form-control select2bs4" name="id_buku" style="width:100%;">
                                                                <?php
                                                                $sqlbuku = "SELECT id as id_buku, kode, judul, penulis, penerbit, tahun_penerbit FROM buku";
                                                                $databuku = $conn->query($sqlbuku);
                                                                foreach($databuku as $hasilbuku){ ?>
                                                                    <option <?=$hasilbuku['id_buku'] == $hasil['id_buku'] ? 'selected':''; ?> value="<?=$hasilbuku['id_buku'];?>"><?php echo $hasilbuku['kode']." - ".$hasilbuku['judul'];?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Pilih Siswa</label>
                                                            <select class="form-control select2bs4" name="id_siswa" style="width:100%;">
                                                                <?php
                                                                $sqlsiswa = "SELECT id as id_siswa, nis, nama, jk, kelas FROM siswa";
                                                                $datasiswa = $conn->query($sqlsiswa);
                                                                foreach($datasiswa as $hasilsiswa){ ?>
                                                                    <option <?=$hasilsiswa['id_siswa'] == $hasil['id_siswa'] ? 'selected':''; ?> value="<?=$hasilsiswa['id_siswa'];?>"><?php echo $hasilsiswa['nis']." - ".$hasilsiswa['nama'];?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Tanggal Pinjam</label>
                                                            <input type="date" id="tanggal_pinjam" class="form-control" name="tanggal_pinjam" value="<?=$hasil['tanggal_pinjam'];?>">
                                                        </div>
                                                        <div class="formgroup">
                                                            <label>Tanggal Kembali</label>
                                                            <input type="date" id="tanggal_kembali" class="form-control" name="tanggal_kembali" value="<?=$hasil['tanggal_kembali'];?>">
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="../api/peminjaman.php?proses=hapus&id=<?=$hasil['idpeminjaman'];?>" class="btn btn-danger" style="margin-top:0px;">Hapus</button>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>      
        </div>
    </div>
</div>