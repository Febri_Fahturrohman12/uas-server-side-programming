<?php
session_start();

if (!isset($_SESSION['username'])) {
    header("Location: ../index.php");
}

include_once "../api/routes.php";
include '../api/koneksi.php';
include "header.php";
?>
<div class="col-lg-2 col-md-2" style="padding-left: 0px;background-color: #313644;height: 100%;float: left;">
    <?php include "sidebar.php"; ?>
</div>
<div class="col-lg-10 col-md-10" style="background-color: #f3f4f3;height: 100%;float: right;">
    <div class="row">
        <div class="col-lg-12 col-md-12" style="background-color: #fff;height: 75px;vertical-align: middle;font-size: 19px;font-weight: 600;color: #727272;">
            <p style="vertical-align: middle;padding-top:25px;">Dashboard</p>
        </div>
        <div class="col-lg-12 col-md-12 mt-4">
            <div class="row mt-2">
                <div class="col-4">
                    <div class="card" style="border: none;box-shadow: 4px 4px 10px #ddd;background-color: #464e62;color: #fff;">
                        <div class="card-body">
                            <div class="row" style="display: flex;">
                                <div class="col-md-6">
                                    <span style="font-size: 19px;vertical-align: middle;">Data Siswa</span>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                <?php
                                    $sql = "SELECT count(*) as total from siswa";
                                    $data = $conn->query($sql);
                                    foreach($data as $hasil){ ?>
                                        <span style="font-size: 22px;"><?=$hasil['total']; ?></span>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" style="border: none;box-shadow: 4px 4px 10px #ddd;background-color: #464e62;color: #fff;">
                        <div class="card-body">
                            <div class="row" style="display: flex;">
                                <div class="col-md-6">
                                    <span style="font-size: 19px;vertical-align: middle;">Data Buku</span>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                <?php
                                    $sql = "SELECT count(*) as total from buku";
                                    $data = $conn->query($sql);
                                    foreach($data as $hasil){ ?>
                                        <span style="font-size: 22px;"><?=$hasil['total']; ?></span>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" style="border: none;box-shadow: 4px 4px 10px #ddd;background-color: #464e62;color: #fff;">
                        <div class="card-body">
                            <div class="row" style="display: flex;">
                                <div class="col-md-6">
                                    <span style="font-size: 19px;vertical-align: middle;">Dipinjam</span>
                                </div>
                                <div class="col-md-6" style="text-align: right;">
                                <?php
                                    $sql = "SELECT count(*) as total from peminjaman";
                                    $data = $conn->query($sql);
                                    foreach($data as $hasil){ ?>
                                        <span style="font-size: 22px;"><?=$hasil['total']; ?></span>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>