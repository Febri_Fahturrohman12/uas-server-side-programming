<div class="row">
    <div class="col-md-12" style="margin: 0px auto;position: relative;padding-top: 50px;background-color: #464e62;">
        <img src="../assets/avatar.png" alt="Avatar" class="avatar" style="vertical-align: middle;width: 60px;height: 60px;border-radius: 50%;position:absolute;margin: auto; left:0;right:0;top:0;bottom:0;"><br>
    </div>
    <div class="col-md-12" style="background-color: #464e62;">
        <p style="text-align: center;color:#fff;font-size: 18px;margin-top: 0px;font-weight: bold;">Selamat Datang Admin</p>
    </div>
    <div class="col-md-12" style="color: #fff;border-bottom: solid 1px #888;">
        <div class="col-md-12" style="padding: 20px 10px 10px 20px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: 400;">
            <a href="dashboard.php" style="text-decoration: none;color: #fff;">Dashboard</a>
        </div>
    </div>
    <div class="col-md-12" style="color: #fff;border-bottom: solid 1px #888;">
        <div class="col-md-12" style="padding: 20px 10px 10px 20px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: 400;">
            <a href="siswa.php" style="text-decoration: none;color: #fff;">Data Siswa</a>
        </div>
    </div>
    <div class="col-md-12" style="color: #fff;border-bottom: solid 1px #888;">
        <div class="col-md-12" style="padding: 20px 10px 10px 20px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: 400;">
            <a href="buku.php" style="text-decoration: none;color: #fff;">Data Buku</a>
        </div>
    </div>
    <div class="col-md-12" style="color: #fff;border-bottom: solid 1px #888;">
        <div class="col-md-12" style="padding: 20px 10px 10px 20px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: 400;">
            <a href="peminjaman.php" style="text-decoration: none;color: #fff;">Peminjaman Buku</a>
        </div>
    </div>
    <div class="col-md-12" style="color: #fff;border-bottom: solid 1px #888;">
        <div class="col-md-12" style="padding: 20px 10px 10px 20px;font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;font-weight: 400;">
        <a href="../api/logout.php" style="text-decoration: none;color: #fff;">Logout</a>
        </div>
    </div>
</div>