<?php 
include_once "api/routes.php"; 
include "assets/head.php"; 
?>

<div class="global-container">
    <div class="card login-form">
        <div class="card-body">
                <h3 class="card-title text-center">Log in</h3>
                <div class="card-text">
                    <form name="logForm" method="post" action="?open=Login">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control form-control-sm" name="username" id="username" aria-describedby="Username">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control form-control-sm" name="password" id="password">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" name="btnLogin">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php"; ?>