Project UAS CRUD Server Side Programming Kelompok :
1. 20201014 Febri Fahturrohman S
2. 20201015 Muhamad Adi Setyawan

DATABASE :
- Import Database yang sudah disediakan uas_perpus.sql
- Apabila konfigurasi user phpMyAdmin berbeda maka ubah terlebih dahulu pada file (api/function.php) & api/koneksi.php
- Jika ingin mengubah nama database, pasitkan ubah konfigurasi terlebih dahulu pada file (api/function.php) & api/koneksi.php
- Ketika import sudah di tersedia data dummy

Direktori :
- Direkomendasikan project ditaruh pada folder xampp/htdocs/uas-server-side-programming
- Project sudah terhubung dengan repo gitlab https://gitlab.com/Febri_Fahturrohman12/uas-server-side-programming

Login :
- username = admin
- password = admin
